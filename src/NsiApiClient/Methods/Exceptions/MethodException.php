<?php declare(strict_types=1);

namespace Hypermed\NsiApiClient\Methods\Exceptions;

use Exception;
use GuzzleHttp;

class MethodException extends Exception
{

}