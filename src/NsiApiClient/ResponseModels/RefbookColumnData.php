<?php


namespace Hypermed\NsiApiClient\ResponseModels;

class RefbookColumnData extends ResponseModel
{
    protected static array $casts
        = [
            'column' => 'string',
            'value'  => 'string',
        ];

    private string  $column;
    private ?string $value;

    public function __toString() : string
    {
        return $this->column . ': ' . $this->value;
    }

    /**
     * @return string
     */
    public function getColumn() : string
    {
        return $this->column;
    }

    /**
     * @param string $column
     * @return RefbookColumnData
     */
    public function setColumn(string $column) : RefbookColumnData
    {
        $this->column = $column;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getValue() : ?string
    {
        return $this->value;
    }

    /**
     * @param string|null $value
     * @return RefbookColumnData
     */
    public function setValue(?string $value) : RefbookColumnData
    {
        $this->value = $value;
        return $this;
    }
}