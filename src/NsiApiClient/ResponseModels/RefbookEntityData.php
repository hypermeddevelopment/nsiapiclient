<?php


namespace Hypermed\NsiApiClient\ResponseModels;

class RefbookEntityData extends ResponseModel
{
    protected static array $casts
        = [
            'data' => 'array[Hypermed\NsiApiClient\ResponseModels\RefbookColumnData]',
        ];

    private array $data = [];

    /**
     * @return array
     */
    public function getData() : array
    {
        return $this->data;
    }

    /**
     * @param array $data
     * @return RefbookEntityData
     */
    public function setData(array $data) : RefbookEntityData
    {
        $this->data = $data;
        return $this;
    }

    protected function _fill(array $data)
    {
        return parent::_fill([
            'data' => $data
        ]);
    }
}