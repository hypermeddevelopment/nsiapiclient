<?php


namespace Hypermed\NsiApiClient\ResponseModels;

class RefbookPageData extends ResponseModel
{
    protected static array $casts
        = [
            'total' => 'int',
            'list' => 'array[Hypermed\NsiApiClient\ResponseModels\RefbookEntityData]',
        ];

    private int $total;
    /** @var array|RefbookColumnData[] */
    private array $list = [];

    /**
     * @return int
     */
    public function getTotal() : int
    {
        return $this->total;
    }

    /**
     * @param int $total
     * @return RefbookPageData
     */
    public function setTotal(int $total) : RefbookPageData
    {
        $this->total = $total;
        return $this;
    }

    /**
     * @return array|RefbookEntityData[]
     */
    public function getList()
    {
        return $this->list;
    }

    /**
     * @param array|RefbookEntityData[] $list
     * @return RefbookPageData
     */
    public function setList($list)
    {
        $this->list = $list;
        return $this;
    }
}